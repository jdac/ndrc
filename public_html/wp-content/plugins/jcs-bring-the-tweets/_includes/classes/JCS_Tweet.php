<?php
/* 
 * Project:    NDRC
 * Class:      JCS_Tweet
 * Created:    Jan 07, 2022 8:01 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description:  The JCS_Tweet class is responsible for
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */


namespace jcs_bring_the_tweets;


class JCS_Tweet {

    protected $created_at;
    protected $id;
    protected $likes;
    protected $quotes;
    protected $replies;
    protected $retweets;
    protected $text;

    private $user;

    public static function reply_svg_path(): string {
        return "M14.046 2.242l-4.148-.01h-.002c-4.374 0-7.8 3.427-7.8 7.802 0 4.098 3.186 7.206 7.465 7.37v3.828c0 .108.044.286.12.403.142.225.384.347.632.347.138 0 .277-.038.402-.118.264-.168 6.473-4.14 8.088-5.506 1.902-1.61 3.04-3.97 3.043-6.312v-.017c-.006-4.367-3.43-7.787-7.8-7.788zm3.787 12.972c-1.134.96-4.862 3.405-6.772 4.643V16.67c0-.414-.335-.75-.75-.75h-.396c-3.66 0-6.318-2.476-6.318-5.886 0-3.534 2.768-6.302 6.3-6.302l4.147.01h.002c3.532 0 6.3 2.766 6.302 6.296-.003 1.91-.942 3.844-2.514 5.176z";
    }

    public static function retweet_svg_path(): string {
        return "M23.77 15.67c-.292-.293-.767-.293-1.06 0l-2.22 2.22V7.65c0-2.068-1.683-3.75-3.75-3.75h-5.85c-.414 0-.75.336-.75.75s.336.75.75.75h5.85c1.24 0 2.25 1.01 2.25 2.25v10.24l-2.22-2.22c-.293-.293-.768-.293-1.06 0s-.294.768 0 1.06l3.5 3.5c.145.147.337.22.53.22s.383-.072.53-.22l3.5-3.5c.294-.292.294-.767 0-1.06zm-10.66 3.28H7.26c-1.24 0-2.25-1.01-2.25-2.25V6.46l2.22 2.22c.148.147.34.22.532.22s.384-.073.53-.22c.293-.293.293-.768 0-1.06l-3.5-3.5c-.293-.294-.768-.294-1.06 0l-3.5 3.5c-.294.292-.294.767 0 1.06s.767.293 1.06 0l2.22-2.22V16.7c0 2.068 1.683 3.75 3.75 3.75h5.85c.414 0 .75-.336.75-.75s-.337-.75-.75-.75z";
    }

    public static function likes_svg_path(): string {
        return "M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z";
    }

    public static function verified_svg_path(): string {
        return "M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z";
    }

    public function __construct(string $id) {
        $this->id = $id;
    }

    public function get_verified_path() {
        return self::verified_svg_path();
    }
    public function get_likes_path() {
        return self::likes_svg_path();
    }
    public function get_replies_path() {
        return self::reply_svg_path();
    }
    public function get_retweets_path() {
        return self::retweet_svg_path();
    }

    public function get_created_at(): string {
        return $this->created_at;
    }
    public function set_created_at(string $datetime) {
        $this->created_at = $datetime;
    }
    public function get_id(): string {
        return $this->id;
    }
    public function get_likes(): int {
        return $this->likes;
    }
    public function set_likes(int $num_likes) {
        $this->likes = $num_likes;
    }
    public function get_quotes(): int {
        return $this->quotes;
    }
    public function set_quotes(int $num_quotes) {
        $this->quotes = $num_quotes;
    }
    public function get_replies(): int {
        return $this->replies;
    }
    public function set_replies(int $num_replies) {
        $this->replies = $num_replies;
    }
    public function get_retweets(): int {
        return $this->retweets;
    }
    public function set_retweets(int $num_retweets) {
        $this->retweets = $num_retweets;
    }
    public function get_tweet(): string {
        return $this->text;
    }
    public function set_tweet(string $the_tweet) {
        // Order is important!
        $text = $this->linkify_links($the_tweet);
        $text = $this->linkify_text_mentions($text);
        $text = $this->linkify_text_hashtags($text);
        $this->text = $text;
    }
    public function set_user(JCS_Twitter_User $the_user) {
        $this->user = $the_user;
    }
    public function get_user() {
        return $this->user;
    }

    public function to_string(): string {
        return $this->id . ':'
            . $this->created_at . ':'
            . $this->likes . ':'
            . $this->quotes . ':'
            . $this->replies . ':'
            . $this->retweets . ':'
            . $this->text;
    }

    private function linkify_links(string $text) {
        $pattern = '@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@';
        $replace = '<a href="$1" target="_blank">$1</a>';
        return preg_replace($pattern, $replace, $text);
    }

    private function linkify_text_mentions(string $text) {
        $pattern = '/@([a-zA-Z0-9_]+)/';
        $replace = '<a href="https://twitter.com/$1" target="_blank">@$1</a>';
        return preg_replace($pattern, $replace, $text);
    }

    private function linkify_text_hashtags(string $text) {
        $pattern = '/#([a-zA-Z0-9_]+)/';
        $replace = '<a href="https://twitter.com/hashtag/$1?src=hashtag_click" target="_blank">#$1</a>';
        return preg_replace($pattern, $replace, $text);
    }

}
