<?php
/* 
 * Project:    NDRC
 * Class:      JCS_Twitter_User
 * Created:    Jan 07, 2022 7:59 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description:  The JCS_Twitter_User class is responsible for
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

namespace jcs_bring_the_tweets;

class JCS_Twitter_User {

    protected string $handle;
    protected string $user_name;
    protected string $name;
    protected bool $verified;
    protected string $profile_img_uri;
    private string $twitter_id;

    public function __construct(string $handle) {
        $this->handle = trim($handle);
        $this->verified = false;
    }

    /**
     * @return string
     */
    public function get_handle(): string {
        return $this->handle;
    }

    /**
     * @return string
     */
    public function get_user_name(): string {
        return $this->user_name;
    }

    public function set_user_name(string $u_name) {
        $this->user_name = $u_name;
    }

    /**
     * @return string
     */
    public function get_profile_img_uri(): string {
        return $this->profile_img_uri;
    }

    public function set_profile_img_uri(string $uri) {
        $this->profile_img_uri = $uri;
    }

    public function get_id() {
        return $this->twitter_id;
    }

    public function set_id(string $t_id) {
        $this->twitter_id = $t_id;
    }

    public function get_name() {
        return $this->name;
    }

    public function set_name(string $name) {
        $this->name = $name;
    }

    public function get_verified() {
        return $this->verified;
    }

    public function set_verified(bool $is_v) {
        $this->verified = $is_v;
    }

    public function to_string(): string {
        return $this->handle . ':'
            . $this->user_name . ':'
            . $this->name . ':'
            . $this->twitter_id . ':'
            . $this->verified . ':'
            . $this->profile_img_uri;
    }



}
