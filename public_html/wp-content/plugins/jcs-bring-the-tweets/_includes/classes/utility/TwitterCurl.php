<?php
/* 
 * Project:    NDRC
 * Class:      TwitterCurl
 * Created:    Jan 07, 2022 8:03 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description:  The TwitterCurl class is responsible for
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */


namespace jcs_bring_the_tweets\utility;

use jcs_bring_the_tweets\JCS_Tweet;
use jcs_bring_the_tweets\JCS_Twitter_User;

class TwitterCurl {

    public function set_user_meta_for(JCS_Twitter_User $user): void
    {
        $ch = $this->curl_init();
        $uri = 'https://' . $this->get_hostname() . 'users/by/username/' . $user->get_handle() . "?user.fields=verified";
        curl_setopt($ch, CURLOPT_URL, $uri);
        $curl_response = curl_exec($ch);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
            $response = json_decode($curl_response);
            $user->set_id($response->data->id);
            $user->set_name($response->data->name);
            $user->set_user_name($response->data->username);
            $user->set_verified($response->data->verified);
            $this->set_profile_img_uri($user);
        }
        curl_close($ch);
    }

    public function get_tweet(JCS_Tweet $the_tweet) {
        $ch = $this->curl_init();
        $uri = "https://". $this->get_hostname() . "tweets/" . $the_tweet->get_id() . "?tweet.fields=public_metrics,created_at";
        curl_setopt($ch, CURLOPT_URL, $uri);
        $curl_response = curl_exec($ch);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
            $response = json_decode($curl_response);
            $the_tweet->set_created_at($response->data->created_at);
            $the_tweet->set_likes($response->data->public_metrics->like_count);
            $the_tweet->set_quotes($response->data->public_metrics->quote_count);
            $the_tweet->set_replies($response->data->public_metrics->reply_count);
            $the_tweet->set_retweets($response->data->public_metrics->retweet_count);
            $the_tweet->set_tweet($response->data->text);
        } else {
            $the_tweet->set_tweet($uri);
        }
        curl_close($ch);
    }

    private function set_profile_img_uri(JCS_Twitter_User $user) {
        $ch = $this->curl_init();
        $uri = 'https://'. $this->get_hostname() . "users/" . $user->get_id() . "?user.fields=profile_image_url";
        curl_setopt($ch, CURLOPT_URL, $uri);
        $curl_response = curl_exec($ch);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
            $response = json_decode($curl_response);
            $user->set_profile_img_uri($response->data->profile_image_url);
        }
        curl_close($ch);
    }

    private function curl_init() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, FALSE);

        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $this->get_bearer_token();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        return $ch;

    }

    private function get_bearer_token(): string {
        return get_option('bearer_token');

    }

    private function get_hostname(): string {
        return 'api.twitter.com/2/';
    }

}
