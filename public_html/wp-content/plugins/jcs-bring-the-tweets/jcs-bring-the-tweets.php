<?php
/* 
 * Plugin Name: JCS Bring the Tweets
 * Plugin URI:  https://bitbucket.org/jdac/ndrc
 * Description: Integrates twitter content with the FLASH NDRC WordPress site
 * Version:     1.0
 * Created:    Jan 07, 2022 7:00 AM
 * Author:     John Arnold
 * Author URI: https://drivejcs.com
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Copyright: 2022 John Arnold <john@jdacsolutions.com>
 *
 * This program is free software; you can distribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY;  without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Se the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

require( 'vendor/autoload.php');
use Doctrine\Inflector\InflectorFactory;

// 1. Set up default plugin settings
// Call function when plugin is activated
register_activation_hook(__FILE__, 'jcs_bring_the_tweets_install');

function jcs_bring_the_tweets_install() {
    // Set up default option values
    $jcs_options_arr = array(
        'default_lan' => 'en-US'
    );
    update_option('jcs_bring_the_tweets_options', $jcs_options_arr);
    flush_rewrite_rules();
}

function test_me():string {
    return "We are In";
}

function get_tweet_for_user_and_id(string $twitter_handle, string $tweet_id): \jcs_bring_the_tweets\JCS_Tweet
{
    /*
     * 1. We're going to create a user object using the twitter handle and API call to twitters.
     * 2. We're going to create a tweet object using the tweet id
     * 3. Attach the user object to the tweet
     * 4. Return the whole shebang
     */
    $user = new \jcs_bring_the_tweets\JCS_Twitter_User($twitter_handle);
    $api = new \jcs_bring_the_tweets\utility\TwitterCurl();
    $api->set_user_meta_for($user);
    $tweet = new \jcs_bring_the_tweets\JCS_Tweet($tweet_id);
    $api->get_tweet($tweet);
    $tweet->set_user($user);
    return $tweet;
}
