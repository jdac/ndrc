<?php
    /*
     * Plugin Name: JCS Extend Folders
     * Plugin URI:  https://bitbucket.org/jdac/ndrc
     * Description: Utilizes the taxonomies created by the Premio Folders plugin to query media library attachments by folder ids.
     * Version:     1.0
     * Created:    JJan 11, 2022 2:50 PM
     * Author:     John Arnold
     * Author URI: https://drivejcs.com
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     * Copyright: 2022 John Arnold <john@jdacsolutions.com>
     *
     * This program is free software; you can distribute it and/or modify it
     * under the terms of the GNU General Public License as published by
     * the Free Software Foundation; either version 2 of the License, or
     * (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful, but
     * WITHOUT ANY WARRANTY;  without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Se the GNU
     * General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License along
     * with this program; if not, write to the Free Software Foundation, Inc.,
     * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
     */

// 1. Set up default plugin settings
// Call function when plugin is activated
register_activation_hook(__FILE__, 'jcs_extend_folders_install');

function jcs_extend_folders_install() {
    // Set up default option values
    $jcs_options_arr = array(
        'default_lan' => 'en-US'
    );
    update_option('jcs_extend_folders_options', $jcs_options_arr);
    flush_rewrite_rules();
}

function extend_folders_confirm():string {
    return "Hello from the JCS Extends Folders plugin";
}

/**
 * Given a taxonomy id (Premio Folder Id, actually), return an array of all media attachments residing
 * in that Premio Media Folder
 */
function get_attachment_ids_for_folder(int $folder_id, int $limit = 0): array {

    global $wpdb;
    $table = $wpdb->prefix . 'term_relationships';
    $fetch_ids = "SELECT `object_id` FROM `" . $table . "` WHERE `term_taxonomy_id` = " . $folder_id . " "
        . "ORDER BY RAND()";
    $fetch_ids .= ($limit) ? " LIMIT " . $limit : '';
    $id_results = $wpdb->get_results($fetch_ids);
    $id_array = [];
    foreach ($id_results as $attachment_id) {
       $id_array[] = $attachment_id->object_id;
    }
    return $id_array;

}



