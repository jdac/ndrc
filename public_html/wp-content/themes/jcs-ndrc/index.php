<?php
    /*
     * Project:    FLASH WP NEW
     * File:       index.php
     * Created:    Oct 09, 2021 08:25
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description: Main template file for theme
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    get_header();
    $is_archive = false;
?>
    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

    <?php while( have_posts() ): the_post(); ?>

        <section id="hero_content">
            <?php get_template_part('parts/single', 'page-hero'); ?>
        </section>

        <section id="page_content" style="min-height: 30rem;">
            <div class="container">
                <div class="row">
                    <div class="col-12 gallery-archive" style="padding: 4rem 0; text-align:center">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
    </section>

<?php endwhile; ?>

    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

<?php
    get_footer();

