<?php
/* 
 * Project:    NDRC
 * File:       archive-event_gallery.php
 * Created:    Jan 07, 2022 9:11 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Archive template for event galleries.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    get_header();
    $first = true;
    $is_archive = true;
?>
    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

    <?php while( have_posts() ) : the_post(); ?>

        <?php if ($first) : ?>
            <section id="hero_content">
                <?php get_template_part('parts/inner', 'page-hero'); ?>
            </section>
            <section id="page_content" class="gallery-section" style="min-height: 30rem;">
        <?php endif; ?>

        <div class="container">
            <div class="row">
                <div class="col-12 gallery-archive">
                    <h2><a href="<?php echo get_the_permalink(); ?>"><?php the_content(); ?></a></h2>
                </div>
            </div>
        </div>
        <?php get_template_part('parts/inner', 'events-gallery'); ?>
        <?php $first = false; ?>
    <?php endwhile; ?>

    </section>

    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

<?php
get_footer();

