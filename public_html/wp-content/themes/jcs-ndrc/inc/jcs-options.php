<?php
/* 
 * Project:    NDRC
 * File:       jcs-options.php
 * Created:    Jan 07, 2022 1:23 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Defines custom theme option for the jcs-ndrc theme
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

function jcs_theme_options() {

    // Main Options Page
    add_menu_page(
        __('NDRC FLASH Options', 'jcs-ndrc'), // page Title
        __('NDRC FLASH Options', 'jcs-ndrc'), // menu Title
        'manage_options', // capability
        'jcs_options', // menu Slug
        'jcs_manage_options', // function to call
        '', // icon url
        20 // menu position
    );
    
    // Submenu Page (7th arg, position, is optional)
    add_submenu_page(
        'jcs_options', // parent
        __('Company Information', 'jcs-ndrc'), // page title
        __('Company Info', 'jcs-ndrc'), // menu title
        'manage_options', // capability
        'flash_company_info', // menu slug
        'jcs_company_info' // callable
    );
    // Google APIS Submenu Page (7th arg, position, is optional)
    add_submenu_page(
        'jcs_options', // parent
        __('Google APIs & Keys', 'jcs-ndrc'), // page title
        __('Google APIs', 'jcs-ndrc'), // menu title
        'manage_options', // capability
        'ndrc_google_apis', // menu slug
        'jcs_google_apis' // callable
    );

    // Twitter APIS Submenu Page (7th arg, position, is optional)
    add_submenu_page(
        'jcs_options', // parent
        __('Twitter APIs & Keys', 'jcs-ndrc'), // page title
        __('Twitter APIs', 'jcs-ndrc'), // menu title
        'manage_options', // capability
        'ndrc_twitter_apis', // menu slug
        'jcs_twitter_apis' // callable
    );
    $role = get_role('editor');
    $role->add_cap('manage_options', true);
}
add_action('admin_menu', 'jcs_theme_options');

/*
 * Register settings
 */
function jcs_options_settings() {
    
    // Footer address info, etc.
    // Company Information
    register_setting('jcs_options_info', 'jcs_info_street');
    register_setting('jcs_options_info', 'jcs_info_street2');
    register_setting('jcs_options_info', 'jcs_info_city');
    register_setting('jcs_options_info', 'jcs_info_state');
    register_setting('jcs_options_info', 'jcs_info_zip');
    register_setting('jcs_options_info', 'jcs_info_phone');
    register_setting('jcs_options_info', 'jcs_info_tollfree');
    register_setting('jcs_options_info', 'jcs_site_email_from');
    register_setting('jcs_options_info', 'jcs_site_email_from_name');
    register_setting('jcs_options_info', 'jcs_contact_email_to');
    register_setting('jcs_options_info', 'jcs_admin_email_to');

    // Google Options
    register_setting('jcs_options_google', 'jcs_is_live');
    register_setting('jcs_options_google', 'jcs_show_surveys');
    register_setting('jcs_options_google', 'jcs_recaptcha_v3_site_key');
    register_setting('jcs_options_google', 'jcs_recaptcha_v3_secret_key');
    register_setting('jcs_options_google', 'jcs_recaptcha_v3_site_key_staging');
    register_setting('jcs_options_google', 'jcs_recaptcha_v3_secret_key_staging');
    register_setting('jcs_options_google', 'jcs_gtag_id');
    register_setting('jcs_options_google', 'jcs_adwords_key');
    register_setting('jcs_options_google', 'jcs_adwords_c_label');
    register_setting('jcs_options_google', 'jcs_maps_key_live');
    register_setting('jcs_options_google', 'jcs_maps_key_staging');

    register_setting('jcs_options_twitter', 'bearer_token');

}
add_action('init', 'jcs_options_settings');

function jcs_manage_options() { ?>
    <div class="wrap">
        <h1>FLASH NDRC Settings</h1>
        <p>The JCS NDRC theme supports the following custom settings:</p>
        <ul style="list-style-type: '- '; padding-left: 3rem;">
            <li><h3>Google APIs & Keys</h3></li>
            <li><h3>Twitter APIs & Keys</h3></li>
        </ul>
        <h2 style='margin-top: 3rem;'>If you require more customization for APIs and other settings, please contact the theme developer at <a href="mailto:webadmin@jdacsolutions.com">webadmin@jdacsolutions.com</a>.</h2>
    </div>
    <?php
}
    
    function jcs_company_info() { ?>
        <div class="wrap">
            <h1><?php echo get_bloginfo(); ?> Company Information</h1>
            <form action="options.php" method="post">
                <?php
                    settings_fields('jcs_options_info');
                    do_settings_sections('jcs_options_info');
                ?>
                <table class="form-table">
                    <tr>
                        <th scope="row"><h2>Address Information</h2></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Street: </th>
                        <td>
                            <input type="text" size="50" name="jcs_info_street" value="<?php echo esc_attr( get_option('jcs_info_street')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Street 2: </th>
                        <td>
                            <input type="text" size="50" name="jcs_info_street2" value="<?php echo esc_attr( get_option('jcs_info_street2')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">City: </th>
                        <td>
                            <input type="text" size="50" name="jcs_info_city" value="<?php echo esc_attr( get_option('jcs_info_city')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">State: </th>
                        <td>
                            <input type="text" name="jcs_info_state" value="<?php echo esc_attr( get_option('jcs_info_state')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Zip: </th>
                        <td>
                            <input type="text" name="jcs_info_zip" value="<?php echo esc_attr( get_option('jcs_info_zip')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Telephone: </th>
                        <td>
                            <input type="text" name="jcs_info_phone" value="<?php echo esc_attr( get_option('jcs_info_phone')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Toll Free: </th>
                        <td>
                            <input type="text" name="jcs_info_tollfree" value="<?php echo esc_attr( get_option('jcs_info_tollfree')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><h2>Email Addresses</h2></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Company From Email Address: </th>
                        <td>
                            <input type="email" size="50" name="jcs_site_email_from" value="<?php echo esc_attr( get_option('jcs_site_email_from')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Company From Email Name: </th>
                        <td>
                            <input type="text" size="50" name="jcs_site_email_from_name" value="<?php echo esc_attr( get_option('jcs_site_email_from_name')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Business Admin Email Address: </th>
                        <td>
                            <input type="email" size="50" name="jcs_contact_email_to" value="<?php echo esc_attr( get_option('jcs_contact_email_to')); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Technical Admin Email Address: </th>
                        <td>
                            <input type="email" size="50" name="jcs_admin_email_to" value="<?php echo esc_attr( get_option('jcs_admin_email_to')); ?>" />
                        </td>
                    </tr>
                </table>
                <?php submit_button(); ?>
            </form>
        </div>
        
        <?php
    }

function jcs_google_apis() { ?>
    <div class="wrap">
        <h1><?php echo get_bloginfo(); ?> Google APIs and Keys</h1>
        <form action="options.php" method="post">
            <?php
            settings_fields('jcs_options_google');
            do_settings_sections('jcs_options_google');
            ?>
            <h2>Toggles</h2>
            <table class="form-table">
                <tr>
                    <th scope="row">Site Run Mode:</th>
                    <td>
                        <select name="jcs_is_live">
                            <option value="0" <?php echo (!get_option('jcs_is_live')) ? 'selected' : ''; ?>>Staging</option>
                            <option value="1" <?php echo (get_option('jcs_is_live')) ? 'selected' : ''; ?>>LIVE</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Show Surveys:</th>
                    <td>
                        <select name="jcs_show_surveys">
                            <option value="1" <?php echo (get_option('jcs_show_surveys')) ? 'selected' : ''; ?>>Yes</option>
                            <option value="0" <?php echo (!get_option('jcs_show_surveys')) ? 'selected' : ''; ?>>No</option>
                        </select>
                    </td>
                </tr>
            </table>
            <h2>ReCaptcha v3 Keys</h2>
            <table class="form-table">
                <tr>
                    <th>Staging Site Key: </th>
                    <td>
                        <input type="text" size="50" name="jcs_recaptcha_v3_site_key_staging" value="<?php echo esc_attr( get_option('jcs_recaptcha_v3_site_key_staging')); ?>" />
                    </td>
                    <th scope="row"> LIVE Site Key: </th>
                    <td>
                        <input type="text" size="50" name="jcs_recaptcha_v3_site_key" value="<?php echo esc_attr( get_option('jcs_recaptcha_v3_site_key')); ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Staging Secret Key: </th>
                    <td>
                        <input type="text" size="50" name="jcs_recaptcha_v3_secret_key_staging" value="<?php echo esc_attr( get_option('jcs_recaptcha_v3_secret_key_staging')); ?>" />
                    </td>
                    <th scope="row">LIVE Secret Key: </th>
                    <td>
                        <input type="text" size="50" name="jcs_recaptcha_v3_secret_key" value="<?php echo esc_attr( get_option('jcs_recaptcha_v3_secret_key')); ?>" />
                    </td>
                </tr>
            </table>
            <h2>Google Maps & Places</h2>
            <table class="form-table">
                <tr>
                    <th>Staging Maps/Places API Key: </th>
                    <td>
                        <input type="text" size="50" name="jcs_maps_key_staging" value="<?php echo esc_attr( get_option('jcs_maps_key_staging')); ?>" />
                    </td>
                    <th scope="row">LIVE Maps/Places API Key: </th>
                    <td>
                        <input type="text" size="50" name="jcs_maps_key_live" value="<?php echo esc_attr( get_option('jcs_maps_key_live')); ?>" />
                    </td>
                </tr>
            </table>
            <h2>Google Ad Words</h2>
            <table class="form-table">
                <tr>
                    <th scope="row">Conversion ID: </th>
                    <td>
                        <input type="text" size="50" name="jcs_adwords_key" value="<?php echo esc_attr( get_option('jcs_adwords_key')); ?>" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Conversion Label: </th>
                    <td>
                        <input type="text" size="50" name="jcs_adwords_c_label" value="<?php echo esc_attr( get_option('jcs_adwords_c_label')); ?>" />
                    </td>
                </tr>
            </table>
            <h2>Google Tag Manager</h2>
            <table class="form-table">
                <tr>
                    <th scope="row">Google Tags ID: </th>
                    <td>
                        <input type="text" size="50" name="jcs_gtag_id" value="<?php echo esc_attr( get_option('jcs_gtag_id')); ?>" />
                    </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}

function jcs_twitter_apis() { ?>
    <div class="wrap">
        <h1><?php echo get_bloginfo(); ?> Twitter APIs and Keys</h1>
        <p>
            In order for the selected tweets to work on the home page, the site requires a bearer token to access
            the Twitter API.  Enter the bearer token below.
        </p>
        <form action="options.php" method="post">
            <?php
            settings_fields('jcs_options_twitter');
            do_settings_sections('jcs_options_twitter');
            ?>
            <h2>Twitter API Bearer Token</h2>
            <table class="form-table">
                <tr>
                    <th scope="row">Enter Token: </th>
                    <td>
                        <input type="text" size="50" name="bearer_token" value="<?php echo esc_attr( get_option('bearer_token')); ?>" />
                    </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}
