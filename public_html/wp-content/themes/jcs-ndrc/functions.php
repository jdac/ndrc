<?php
    /*
     * Project:    FLASH WP NDRC
     * File:       functions.php
     * Created:    Dev 18, 2021 09:54
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description: Theme function file for the JCS NDRC Theme.
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */

    // Handles Theme admin options
    if (file_exists(get_template_directory() . '/inc/jcs-options.php')) {
        require_once(get_template_directory() . '/inc/jcs-options.php');
    }
    // Handles Theme Forms
    if (file_exists(get_template_directory() . '/inc/jcs-forms.php')) {
        require_once(get_template_directory() . '/inc/jcs-forms.php');
    }
    // JCS Widgets
    if (file_exists(get_template_directory() . '/inc/jcs-widgets.php')) {
        require_once(get_template_directory() . '/inc/jcs-widgets.php');
    }

    /*
     * Theme Support
     */
    function jcs_theme_support() {

        // Turn off admin bar on front end while logged in
        add_filter('show_admin_bar', '__return_false');

        // Features
        add_theme_support('title-tag');  // Auto handle page names in browser
        add_theme_support('post-thumbnails');  // Allow featured images

        // Localisation
        load_theme_textdomain('jcs-ndrc', get_template_directory() . '/languages');

        // Do your image sizes
        add_image_size('jcs-hero', 1145, 409, array('left','top'));  // True crops.  You can also specify hard crops telling where to crop.  See function reference.
        add_image_size('jcs-video-img', 630, 375, true);  // Front page video thumbnails
        add_image_size('jcs-video-thumb', 250,150,true); // Front page disaster images
        add_image_size('jcs-hotel-thumb', 345, 195, true);
        // Resize the standard WP thumbnail
        update_option('thumbnail_size_w', 253);
        update_option('thumbnail_size_h', 164);
        update_option('thumbnail_crop', array('left', 'top'));

    }
    add_action('after_setup_theme', 'jcs_theme_support');

    // Make theme custom image sizes available for selection in the back end
    function jcs_custom_sizes( $sizes ): array {
        // Add a line to the array for each size you want available in admin
        return array_merge( $sizes, array(
            'jcs-hero' => __('Hero Image', 'jcs-ndrc'),
            'jcs-video-img' => __('Video Image', 'jcs-ndrc'),
            'jcs-video-thumb' => __('Video Thumbnail', 'jcs-ndrc'),
            'jcs-hotel-thumb' => __('Hotel Thumb', 'jcs-ndrc'),
      ));
    }
    add_filter('image_size_names_choose', 'jcs_custom_sizes');

    /*
     * Theme Styles
     */
    function jcs_enqueue_styles() {
        // Normalize
        wp_register_style('normalize', get_template_directory_uri() . '/assets/css/normalize.css', array(), '8.0.1', 'all');
        wp_enqueue_style('normalize');

        // Bootstrap 4.5.0
        wp_register_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', array(), '4.5.0', 'all');
        wp_enqueue_style('bootstrap');

        // Google Fonts
        wp_register_style('Source-Sans-Pro', 'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap', array(), '1.0', 'all');
        wp_enqueue_style('Source-Sans-Pro');

        // Font Awesome 5.15
        wp_register_style('fontawesome', get_template_directory_uri() . '/assets/css/all.min.css', array(), '5.15', 'all');
        wp_enqueue_style('fontawesome');

        // FluidBox
        wp_register_style('fluidbox', get_template_directory_uri() . '/assets/css/fluidbox.min.css', array(), '2.0.5');
        wp_enqueue_style('fluidbox');

        wp_register_style('royal-slider', get_template_directory_uri() . '/assets/royalslider/royalslider.css', array(), '1.05','all');
        wp_enqueue_style('royal-slider');
        wp_register_style('royal-slider-skin', get_template_directory_uri() . '/assets/royalslider/skins/default/rs-default.css', array(), '1.05','all');
        wp_enqueue_style('royal-slider-skin');

        // JCS Service Main Style
        //wp_register_style('jcs-ndrc-style', get_template_directory_uri() . '/assets/css/style.min.css', array(), filemtime(get_template_directory() . '/assets/css/style.min.css'), 'all');
        wp_register_style('jcs-ndrc-style', get_template_directory_uri() . '/assets/css/style.css', array(), filemtime(get_template_directory() . '/assets/css/style.css'), 'all');
        wp_enqueue_style('jcs-ndrc-style');

    }
    add_action('wp_enqueue_scripts', 'jcs_enqueue_styles');

    /*
     * Theme scripts - generally loaded at the bottom of the page
     */
    function jcs_enqueue_scripts() {

        // WordPress pre-installed
        wp_enqueue_script('jquery');

        // BootStrap 4.5.0
        wp_register_script('bootstrapJS', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array('jquery'), '4.5.0', true);
        wp_enqueue_script('bootstrapJS');

        // FluidBox
        wp_register_script('debounce', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js', array('jquery'), '1.1.0', true);
        wp_register_script('fluidboxjs', get_template_directory_uri() . '/assets/js/jquery.fluidbox.min.js', array('jquery', 'debounce'), '2.0.5', true);
        wp_enqueue_script('debounce');
        wp_enqueue_script('fluidboxjs');

        wp_register_script('rslider', get_template_directory_uri() . '/assets/royalslider/jquery.royalslider.custom.min.js', array('jquery'), '1.05',true);
        wp_enqueue_script('rslider');

        // Theme main script file
        wp_register_script('jcs-js', get_template_directory_uri() . '/assets/js/jcs-scripts.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('jcs-js');
    }
    add_action('wp_enqueue_scripts', 'jcs_enqueue_scripts');

    // Only Load these scripts on the front end (header.php)
    function jcs_header_scripts() {

        if ( $GLOBALS['pagenow'] != 'wp-login.php' && !is_admin() ) {
            // Needed for SVG Handling
            wp_register_script('conditionizer', get_template_directory_uri() . '/assets/js/conditionizr-4.3.0.min.js', array(), '4.3.0', false); // Conditionizr
            wp_enqueue_script('conditionizer'); // Enqueue it!
            wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/modernizr-2.7.1.min.js', array('jquery'), '2.7.1', false); // Modernizr
            wp_enqueue_script('modernizr'); // Enqueue it!
        }
    }
    add_action('init', 'jcs_header_scripts');

    /*
     * Conditional Scripts
     */
    //function jcs_conditional_scripts() {

        //if ( is_singular('peril') ) {
        //    wp_register_script('jcs-perils', get_template_directory_uri() . '/assets/js/jcs-perils.js', array('jquery'), '1.0.0', true);
        //    wp_enqueue_script('jcs-perils');
        //}
    //}
    //add_action('wp_enqueue_scripts', 'jcs_conditional_scripts');

    /*
     * Register Theme Menus. Two provided - Main and Footer. Add as many as you want.
     */
    function jcs_menus() {
        register_nav_menus( array(
            'main_nav' => __('Main Nav Menu', 'jcs-ndrc')
        ));
        register_nav_menus( array(
            'footer_menu' => __('Footer Menu', 'jcs-ndrc')
        ));
        register_nav_menus( array(
            'social_menu' => __('Social Menu', 'jcs-ndrc')
        ));
    }
    add_action('init', 'jcs_menus');

    /**
     * Add category capability to attachments
     */
    function add_categories_to_attachments() {
        register_taxonomy_for_object_type('category', 'attachment');
    }
    //add_action('init', 'add_categories_to_attachments');
    function add_tags_to_attachments() {
        register_taxonomy_for_object_type('post_tag', 'attachment');
    }
    //add_action('init', 'add_tags_to_attachments');
    
    // FLASH Tweets Admin Screen changes
    function define_ndrc_tweet_posts_columns($columns) : array {
        unset($columns['date']);
        unset($columns['ssid']);
        return array_merge($columns,
            [
                'twitter_handle' => __('Twitter Handle', 'jcs-ndrc'),
                'tweet_id' => __('Tweet ID', 'jcs-ndrc'),
                'date' => __('Date', 'jcs-flash'),
                'ssid' => __('ID', 'jcs-flash')
            ]
        );
    }
    add_filter('manage_ndrc_tweet_posts_columns', 'define_ndrc_tweet_posts_columns');
    
    function jcs_tweets_columns($column_key, $post_id) {
        //echo $column_key . "<br />";
        if ( $column_key == 'twitter_handle' ) {
            _e(get_field('twitter_handle', $post_id), 'jcs-ndrc');
        }
        if ($column_key == 'tweet_id') {
            _e(get_field('tweet_id',$post_id), 'jcs-ndrc');
        }
    }
    add_action('manage_ndrc_tweet_posts_custom_column', 'jcs_tweets_columns', 10, 2);
    
    // FLASH Sponsors Admin Screen changes
    function define_ndrc_sponsor_posts_columns($columns) : array {
        unset($columns['date']);
        unset($columns['ssid']);
        return array_merge($columns,
            [
                'level' => __('Sponsor Level', 'jcs-ndrc'),
                'date' => __('Date', 'jcs-flash'),
                'ssid' => __('ID', 'jcs-flash')
            ]
        );
    }
    add_filter('manage_ndrc_sponsor_posts_columns', 'define_ndrc_sponsor_posts_columns');
    
    function jcs_sponsors_columns($column_key, $post_id) {
        //echo $column_key . "<br />";
        if ( $column_key == 'level' ) {
            _e(get_field('sponsor_level', $post_id)['label'], 'jcs-ndrc');
        }
    }
    add_action('manage_ndrc_sponsor_posts_custom_column', 'jcs_sponsors_columns', 10, 2);
    
