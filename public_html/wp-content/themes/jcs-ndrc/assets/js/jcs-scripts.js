/*
 * Project:    Tornado Strong
 * File:       jcs-scripts.js
 * Created:    Mar 29, 2021 2:42:23 PM
 * Updated:
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://jdacsolutions.com
 *
 * Description:  Provides main js functionality for Tornado Strong
 *               jcs-tornado-strong WP Theme
 *
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

$ = jQuery.noConflict();

$(document).ready(function($) {
    console.log("We're Loaded");
    let sliderHeight = 520;
    let sliderWidth = 950;
    let mq = window.matchMedia( "(min-width: 1280px)" );
    if (!mq.matches) {
        sliderHeight = 480;
    }
    $("#fp_videos").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 4,
        autoScaleSliderHeight: 2.5,
        imageScaleMode: 'fill',
        //imageScalePadding: 0,
        autoPlay: {
            enabled: false
        },
        loop: false,
        arrowsNav: false,
        transitionType: 'fade',
        //controlNavigation: 'thumbnails',
        controlNavigation: 'none',
        thumbs: {
            spacing: 15,
            fitInViewport: false,
            autoCenter: true,
            arrowLeft: $('#fp_thumb_left'),
            arrowRight: $('#fp_thumb_right')
        },
        globalCaption: true
    });

    $("#tweets_rs").royalSlider({
        // options go here
        // as an example, enable keyboard arrows nav
        keyboardNavEnabled: true,
        //autoScaleSlider: true,
        //autoScaleSliderWidth: 10,
        //autoScaleSliderHeight: 7.5,
        //imageScaleMode: 'fill',
        //imageScalePadding: 0,
        //slideSpacing: 2,
        autoPlay: {
            enabled: true,
            delay: 5000
        },
        loop: true,
        arrowsNav: false,
        //transitionType: 'fade',
        controlNavigation: 'none',
        globalCaption: false
    });

    //FluidBox Plug-in
    $('.gallery a').each(function() {
        $(this).attr({'data-fluidbox' : ''});
    });

    if($('[data-fluidbox]').length > 0) {
        $('[data-fluidbox]').fluidbox();
    }

});

// Toggles for accordion sections
$('#pop_m_tk').on('click', function() {
    toggle_content('#' + $(this).attr('id'),'#meteoro_content');
});
$('#pop_pc').on('click', function() {
    toggle_content('#' + $(this).attr('id'),'#podcasts_content');
});
$('#pop_s_tk').on('click', function() {
    toggle_content('#' + $(this).attr('id'),'#social_content');
});
$('#expander').on('click', function() {
    let link = $(this).text();
    //console.log(link);
    if ('Expand All' === link) {
        $('#meteoro_content').addClass('show');
        $('#pop_m_tk div.arrow').addClass('open');
        $('#podcasts_content').addClass('show');
        $('#pop_pc div.arrow').addClass('open');
        $('#social_content').addClass('show');
        $('#pop_s_tk div.arrow').addClass('open');
        $(this).text('Close All');
    } else {
        $('#meteoro_content').removeClass('show');
        $('#pop_m_tk div.arrow').removeClass('open');
        $('#podcasts_content').removeClass('show');
        $('#pop_pc div.arrow').removeClass('open');
        $('#social_content').removeClass('show');
        $('#pop_s_tk div.arrow').removeClass('open');
        $(this).text('Expand All');
    }
});
function toggle_content(button, content_div) {
    let this_div = $(content_div);
    let btn_id = button + ' div.arrow';
    //console.log(btn_id);
    let btn = $(btn_id);
    if (this_div.hasClass('show')) {
        this_div.removeClass('show');
        btn.removeClass('open');
    } else {
        this_div.addClass('show');
        btn.addClass('open');
    }
}
// Copy stuff
$("[id^=link_copy_]").on('click', function() {
    let target = '#' + $(this).attr('id').substring(5); // all paragraph ids are copy_nnn matching link_copy_nnn
    let input = document.createElement("textarea");
    //adding p tag text to textarea
    input.value = $(target).text();
    document.body.appendChild(input);
    input.select();
    document.execCommand("Copy");
    // removing textarea after copy
    input.remove();
});
