<?php
    /*
     * Project:    FLASH WP NDRC
     * File:       front-page.php
     * Created:    Dec 18, 2021 10:11
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description: Template for the site front page.
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    get_header();
    global $page_id;
?>
    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

    <section id="hero_content">
        <?php while( have_posts() ): the_post(); $page_id = get_the_ID(); ?>
            <?php get_template_part('parts/front', 'page-hero'); ?>
        <?php endwhile; ?>
    </section>

    <section id="videos">
        <?php get_template_part('parts/content', 'video-slider'); ?>
    </section>

    <!--<section id="tweets">
        <?php //get_template_part('parts/content', 'tweets'); ?>
    </section>-->

    <section id="hotels">
        <?php get_template_part('parts/content', 'hotels'); ?>
    </section>

    <section id="event_gallery">
        <?php get_template_part('parts/content', 'events-gallery'); ?>
    </section>

    <!-- Presenting Partners Logos -->
    <?php
        $p_level = 'presenter';
        get_template_part('parts/content', 'presenters');
    ?>

    <!-- Sponsors -->
    <?php
        $args = array(
            'post_type' => 'ndrc_sponsor',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                'key' => 'sponsor_level',
                'value' => $p_level, // presenter
                'compare' => '!='
                )
            )
        );
        $sponsors = new WP_Query($args);
        $count = $sponsors->post_count;
        if ($count > 0):
    ?>
    <section id="sponsors">
        <div class="container">
            <div class="row">
                <div class="col-12 section-heading centered">
                    <h3>Thank You to Our NDRC <?php echo date("Y"); ?> Sponsors</h3>
                </div>
            </div>
            <div class="row sponsors">
                <?php
                    $p_level = 'titanium';
                    get_template_part('parts/content', 'sponsors');
                    $p_level = 'platinum';
                    get_template_part('parts/content', 'sponsors');
                    $p_level = 'gold';
                    get_template_part('parts/content', 'sponsors');
                    $p_level = 'silver';
                    get_template_part('parts/content', 'sponsors');
                    $p_level = 'bronze';
                    get_template_part('parts/content', 'sponsors');
                    $p_level = 'general';
                    get_template_part('parts/content', 'sponsors');
                ?>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <section class="content-separator">
        <?php get_template_part('parts/content', 'separator'); ?>
    </section>

<?php
    get_footer();

