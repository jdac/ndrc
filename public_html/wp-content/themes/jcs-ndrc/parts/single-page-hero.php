<?php
/* 
 * Project:    NDRC
 * File:       single-page-hero.php
 * Created:    Jan 18, 2022 1:36 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: simple hero template with no text over featured image
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    $page_id = get_the_ID();
    $img_uri = get_the_post_thumbnail_url($page_id, 'jcs-hero');
?>
<div class="container">
    <div class="row">
        <div class="col-12 hero-inner">
            <div id="hero" style="background-image: url(<?php echo $img_uri; ?>);"></div>
        </div>
    </div>
</div>
