<?php
/* 
 * Project:    NDRC
 * File:       content-events-gallery.php
 * Created:    Jan 05, 2022 11:53 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering event photo galleries
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

    $args = array(
        'post_type' => 'event_gallery',
        'meta_key' => 'show_on_front_page',
        'meta_value' => 'yes'
    );
    $attachments = new WP_Query($args);

?>
<div class="container">
    <div class="row">
        <div class="col-12 section-heading centered">
            <?php while ($attachments->have_posts()): $attachments->the_post();
                $folder_id = get_field('media_folder')[0];
            ?>
                <h3>Event Gallery</h3>
                <h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_content(); ?></a></h4>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div class="col-12 gallery-wrap">
            <?php $attachment_ids = get_attachment_ids_for_folder($folder_id, 8); ?>
            <div id="gallery_1" class="gallery gallery-id-14 gallery-columns-9 gallery-size-thumbnail">
                <?php
                    foreach ($attachment_ids as $attachment_id) :
                        $thumb_url = wp_get_attachment_image_url($attachment_id, 'thumbnail');
                        $img_url = wp_get_attachment_image_url($attachment_id, 'large');
                ?>
                    <dl class="gallery-item">
                        <dt class="gallery-icon landscape">
                            <div class="photo-blog">
                                <a href="<?php echo $img_url; ?>">
                                    <img class="polaroid" width="253" height="164" src="<?php echo $thumb_url; ?>" class="attachment-thumbnail size-thumbnail" alt="" />
                                </a>
                            </div>
                        </dt>
                    </dl>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

