<?php
/* 
 * Project:    NDRC
 * File:       inner-events-gallery.php
 * Created:    Jan 13, 2022 8:25 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for display event gallery images on interior pages.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    global $is_archive;
    $folder_id = get_field('media_folder')[0];
    if ($is_archive) {
        $img_limit = 8;
    } else {
        $img_limit = 0;
    }
?>
<div class="container">
    <div class="row">
        <div class="col-12 section-heading inner-gallery">
            <?php if ($is_archive) : ?>
                <h3><a href="<?php echo get_the_permalink(); ?>">View the full gallery</a></h3>
            <?php else : ?>
                <h3><a href="<?php echo get_site_url() . '/galleries'; ?>">View all galleries</a></h3>
            <?php endif; ?>
        </div>
        <div class="col-12 gallery-wrap">
            <?php $attachment_ids = get_attachment_ids_for_folder($folder_id, $img_limit); ?>
            <div id="gallery_<?php echo $folder_id; ?>" class="gallery gallery-id-14 gallery-columns-9 gallery-size-thumbnail">
                <?php
                foreach ($attachment_ids as $attachment_id) :
                    $thumb_url = wp_get_attachment_image_url($attachment_id, 'thumbnail');
                    $img_url = wp_get_attachment_image_url($attachment_id, 'large');
                    ?>
                    <dl class="gallery-item">
                        <dt class="gallery-icon landscape">
                            <div class="photo-blog">
                                <a href="<?php echo $img_url; ?>">
                                    <img class="polaroid" width="253" height="164" src="<?php echo $thumb_url; ?>" class="attachment-thumbnail size-thumbnail" alt="" />
                                </a>
                            </div>
                        </dt>
                    </dl>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>


