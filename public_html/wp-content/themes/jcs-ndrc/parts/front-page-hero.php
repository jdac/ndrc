<?php
/* 
 * Project:    NDRC
 * File:       front-page-hero.php
 * Created:    Jan 05, 2022 11:42 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering hero and front page content on home page.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    $page_id = get_the_ID();
    $img_uri = get_the_post_thumbnail_url($page_id, 'jcs-hero');
?>
<div class="container">
    <div class="row">
        <div class="col-12 d-md-none hero">
            <div id="hero_xs" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/NDRC-2024-600x343.png'; ?>);"></div>
        </div>
        <div class="col-md-12 d-none d-md-block hero">
            <div id="hero" style="background-image: url(<?php echo $img_uri; ?>);"></div>
        </div>
        <div class="col-12 content">
            <div class="front-content">
                <?php echo get_the_content(); ?>
            </div>
            <div class="col-12 prep-download">
                <a href="<?php echo get_field('become_a_sponsor_link'); ?>" target="_blank">Become a Sponsor</a>
            </div>
        </div>
    </div>
</div>
