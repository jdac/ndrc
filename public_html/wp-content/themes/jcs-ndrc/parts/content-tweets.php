<?php
/* 
 * Project:    NDRC
 * File:       content-tweets.php
 * Created:    Jan 06, 2022 10:59 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering tweets on pages.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */

    $args = array(
        'post_type' => 'ndrc_tweet',
        'posts_per_page' => 10,
        'post_status' => 'publish',
        'meta_key' => 'show_on_site',
        'meta_value' => 'yes'
    );
    $tweets = new WP_Query($args);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1 tweet-wrapper">
            <h3>#NDRC Tweets</h3>
            <div id="tweets_rs" class="royalSlider rsDefault">
                <?php while( $tweets->have_posts() ): $tweets->the_post();
                    $the_tweet = get_tweet_for_user_and_id(get_field('twitter_handle'), get_field('tweet_id'));
                    $the_user = $the_tweet->get_user();
                    $created_at = new DateTime($the_tweet->get_created_at());
                    if ($the_user->get_user_name()):
                ?>
                <div class="single-tweet">
                    <div class="jcs-author-box">
                        <div class="jcs-author-box-link">
                            <a href="https://twitter.com/<?php echo $the_user->get_user_name(); ?>" class="jcs-author-avatar" target="_blank" rel="noopener noreferrer">
                                <img src="<?php echo $the_user->get_profile_img_uri(); ?>" alt="<?php echo $the_user->get_user_name(); ?> avatar" width="48" height="48" class="lazyloaded" data-ll-status="loaded">
                                <noscript>
                                    <img src="<?php echo $the_user->get_profile_img_uri(); ?>" alt="<?php echo $the_user->get_user_name(); ?> avatar" width="48" height="48">
                                </noscript>
                            </a>
                            <a href="https://twitter.com/<?php echo $the_user->get_user_name(); ?>" target="_blank" rel="noopener noreferrer" class="jcs-author-name"><?php echo $the_user->get_name(); ?></a>
                            <?php if ($the_user->get_verified() && false) : ?>
                                <span class="jcs-verified">
                                    <svg class="svg-inline--fa fa-check-circle fa-w-16"
                                         aria-hidden="true"
                                         aria-label="verified"
                                         data-fa-processed=""
                                         data-prefix="fa"
                                         data-icon="check-circle"
                                         role="img"
                                         xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 512 512">
                                        <path fill="currentColor" d="<?php echo $the_tweet->get_verified_path(); ?>"></path>
                                    </svg>
                                </span>
                            <?php endif; ?>
                            <div class="tweet-meta">
                                <a href="https://twitter.com/<?php echo $the_user->get_user_name(); ?>" class="jcs-author-screen-name" target="_blank" rel="noopener noreferrer">@<?php echo $the_user->get_user_name(); ?></a>
                                <span class="screen-name-sep">·</span>
                                <a href="https://twitter.com/<?php echo $the_user->get_user_name(); ?>/status/<?php echo $the_tweet->get_id(); ?>" class="jcs-tweet-date" target="_blank" rel="noopener noreferrer"><?php echo $created_at->format('Y-m-d H:i:s'); ?><span class="jcs-screenreader"> <?php echo $the_tweet->get_id(); ?></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="tweet-content">
                        <p><?php echo $the_tweet->get_tweet(); ?></p>
                    </div>
                    <div class="tweet-actions">
                        <div class="action-item">
                            <span class="jcs-verified">
                                <svg class="svg-inline--fa fa-w-16"
                                     viewBox="0 0 24"
                                     aria-hidden="true"
                                     aria-label="like"
                                     role="img"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <g><path fill="currentColor" d="<?php echo $the_tweet->get_likes_path(); ?>"></path></g>
                                </svg>
                            </span>
                            <?php echo $the_tweet->get_likes(); ?>
                        </div>
                        <div class="action-item">
                            <span class="jcs-verified">
                                <svg class="svg-inline--fa fa-w-16"
                                     viewBox="0 0 24"
                                     aria-hidden="true"
                                     aria-label="reply"
                                     role="img"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <g><path fill="currentColor" d="<?php echo $the_tweet->get_replies_path(); ?>"></path></g>
                                </svg>
                            </span>
                            <?php echo $the_tweet->get_replies(); ?>
                        </div>
                        <div class="action-item">
                            <span class="jcs-verified">
                                <svg class="svg-inline--fa fa-w-16"
                                     viewBox="0 0 24"
                                     aria-hidden="true"
                                     aria-label="retweet"
                                     role="img"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <g><path fill="currentColor" d="<?php echo $the_tweet->get_retweets_path(); ?>"></path></g>
                                </svg>
                            </span>
                            <?php echo $the_tweet->get_retweets(); ?>
                        </div>
                    </div>
                </div>
                <?php endif; endwhile; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>

