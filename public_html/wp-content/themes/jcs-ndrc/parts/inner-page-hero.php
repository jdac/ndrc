<?php
/* 
 * Project:    NDRC
 * File:       inner-page-hero.php
 * Created:    Jan 13, 2022 7:36 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: template part for rendering hero on interior pages.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    global $is_archive;
    $page_id = get_the_ID();
    $img_uri = get_the_post_thumbnail_url($page_id, 'jcs-hero');
?>
    <div class="container">
        <div class="row">
            <div class="col-12 hero-inner">
                <div id="hero" style="background-image: url(<?php echo $img_uri; ?>);">
                    <div class="page-title <?php echo ($is_archive) ? '': 'long-title'; ?>">
                        <h1><?php echo ($is_archive) ? 'Event Galleries' : get_the_content(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
