<?php
/* 
 * Project:    NDRC
 * File:       content-sponsors.php
 * Created:    Jan 05, 2022 11:51 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering sponsor logos
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    global $p_level;
    $headers = array(
        'titanium' => 'Titanium Sponsor',
        'platinum' => 'Platinum Sponsor',
        'gold' => 'Gold Sponsor',
        'silver' => 'Silver Sponsor',
        'bronze' => 'Bronze Sponsor',
        'general' => 'General Sponsor'
    );
    $args = array(
        'post_type' => 'ndrc_sponsor',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'sponsor_level',
                'value' => $p_level,
                'compare' => '='
            )
        )
    );
    $sponsors = new WP_Query($args);
    $count = $sponsors->post_count;
?>

<?php if ($count > 0) : ?>
    <div class="col-12 col-md-10 offset-md-1">
        <?php if ('presenter' != $p_level): ?>
            <h4><span class="<?php echo $p_level; ?>"><?php echo ($count > 1) ? $headers[$p_level] . 's' : $headers[$p_level]; ?></span></h4>
        <?php endif; ?>
            <div class="logo-spreader">
                <?php while( $sponsors->have_posts() ) :
                    $sponsors->the_post();
                    $the_link = get_field('link');
                    $img_uri = get_field('logo');
                    ?>
                    <div><a href="<?php echo $the_link; ?>" target="_blank"><img src="<?php echo $img_uri; ?>" alt="<?php echo get_the_title(); ?>" /></a></div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
    </div>
<?php endif; ?>
