<?php
    /* 
     * Project:    ndrc
     * File:       content-presenters.php
     * Created:    Sep 13, 2022 8:01 AM
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     * 
     * Description: Template part for rendering presenting partners on the front page.
     * 
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     * 
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
    global $p_level;
    $args = array(
        'post_type' => 'ndrc_sponsor',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'sponsor_level',
                'value' => $p_level,
                'compare' => '='
            )
        )
    );
    $sponsors = new WP_Query($args);
    $count = $sponsors->post_count;
?>
<?php if ($count > 0) : ?>
    <section id="sponsors">
        <div class="container">
            <div class="row">
                <div class="col-12 section-heading centered">
                    <h3>Presenting Partners</h3>
                </div>
            </div>
            <div class="row sponsors">
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="logo-spreader presenters">
                        <?php while( $sponsors->have_posts() ) :
                    $sponsors->the_post();
                    $the_link = get_field('link');
                    $img_uri = get_field('logo');
                    ?>
                            <div><a href="<?php echo $the_link; ?>" target="_blank"><img src="<?php echo $img_uri; ?>" alt="<?php echo get_the_title(); ?>" /></a></div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;