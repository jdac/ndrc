<?php
/* 
 * Project:    NDRC
 * File:       content-video-slider.php
 * Created:    Jan 05, 2022 2:42 PM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering the "sizzle reel" on the front page
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    global $page_id;
    $args = array(
        'post_type' => 'flash_video',
        'posts_per_page' => 1,
        'meta_query' => array(
            array(
                'key' => 'show_on_front_page',
                'value' => 'yes',
                'compare' => '='
            )
        )
    );
    $vids = new WP_Query($args);
    $num_vids = $vids->post_count;
?>
<div class="container">
    <?php while( $vids->have_posts() ) : $vids->the_post(); ?>
        <div class="row">
            <div class="col-12 section-heading centered">
                <h3><?php echo get_field('video_header'); ?></h3>
            </div>
        </div>
    <?php endwhile; wp_reset_postdata(); ?>
        <div class="row fp_video_wrapper">
            <div id="fp_videos" class="royalSlider rsDefault col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                <?php while( $vids->have_posts() ) : $vids->the_post();
                    $img_uri = get_the_post_thumbnail_url(get_the_ID(), 'jcs-video-img');
                    $thumb_uri = get_the_post_thumbnail_url(get_the_ID(), 'jcs-video-thumb');
                    $vid_link = get_field('video_link');
                    $caption = get_field('caption');
                    ?>
                    <div class="rsContent">
                        <img class="rsImg" src="<?php echo $img_uri; ?>" data-rsVideo="<?php echo $vid_link; ?>" />
                        <div class="rsCaption"><?php echo $caption; ?></div>
                        <div class="rsTmb">
                            <img src="<?php echo $thumb_uri; ?>" />
                            <div class="thumb-cap"><p><?php echo $caption; ?></p></div>
                        </div>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <div id="fp_thumb_left" style="display: none;"></div>
            <div id="fp_thumb_right" style="display: none;"></div>
        </div>
</div>
