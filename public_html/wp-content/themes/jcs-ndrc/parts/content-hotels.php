<?php
/* 
 * Project:    NDRC
 * File:       content-hotels.php
 * Created:    Jan 05, 2022 11:48 AM
 * Author:     John Arnold <john@jdacsolutions.com>
 * Author URI: https://drivejcs.com
 * 
 * Description: Template part for rendering conference hotel information.
 * 
 * License:     GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * To change this template file, choose Settings | Editor | File and Code Templates
 */
    $args = array(
        'post_type' => 'conference_hotel',
        'posts_per_page' => 3,
        'post_status' => 'publish'
    );
    $hotels = new WP_Query($args);
?>
<div class="container">
    <div class="row">
        <div class="col-12 section-heading centered">
            <h3>Conference Hotel</h3>
        </div>
        <?php while( $hotels->have_posts() ): $hotels->the_post();
            $post_id = get_the_ID();
            $img_uri = get_the_post_thumbnail_url($post_id, 'jcs-hotel-thumb');
            $open = get_field('open') == 'yes';
            $header_class = get_field('banner_background');
        ?>
            <div class="col-12 col-lg-4">
                <div class="lodging-wrapper">
                    <div class="lodging" style="background-image: url(<?php echo $img_uri; ?>);"></div>
                    <div class="info">
                        <h4 class="<?php echo $header_class; ?>"><?php echo get_the_title(); ?></h4>
                        <div class="info-content">
                            <?php if ($open) {
                                the_content();
                            } else {
                                echo '<p>Reservations opening soon</p>';
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata() ?>
    </div>
</div>
