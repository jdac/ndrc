<?php
    /*
     * Project:    FLASH WP NDRC
     * File:       footer.php
     * Created:    Dec 18, 2021 10:18
     * Author:     John Arnold <john@jdacsolutions.com>
     * Author URI: https://drivejcs.com
     *
     * Description: Site main footer template
     *
     * License:     GNU General Public License v2 or later
     * License URI: http://www.gnu.org/licenses/gpl-2.0.html
     *
     * To change this template file, choose Settings | Editor | File and Code Templates
     */
?>
    <footer class="site-footer">

        <div class="container">

            <div class="row">
                <div class="col-12 footer-nav">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_menu',
                            'container' => 'div',
                            'container_id' => 'menu_footer',
                            'container_class' => 'menu',
                            'menu_id' => 'footer_nav',
                            'menu_class' => 'menu_items',
                            'depth' => 1,
                            'fallback_cb' => '',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                        ));
                    ?>
                </div> <!-- .footer-nav -->

                <div class="col-12 footer-social-nav">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'social_menu',
                            'container' => 'div',
                            'container_id' => 'menu_footer_social',
                            'container_class' => 'menu',
                            'menu_id' => 'footer_social_nav',
                            'menu_class' => 'menu_items',
                            'link_before' => '<span class="sr-text">',
                            'line_after' => '</span>',
                            'depth' => 1,
                            'fallback_cb' => '',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
                        ));
                    ?>
                </div> <!-- .footer-nav -->
                <div class="col-12 footer-logo">
                    <a href="https://nationaldisasterresilienceconference.org"><img src="<?php echo get_template_directory_uri() . '/assets/images/logos/NDRC24_1C_White_Logo-190x82.png'; ?>" alt="<?php echo bloginfo(); ?>" /></a>
                </div> <!-- .footer-logo -->
                <div class="col-12 footer-copyright">
                    <p>Copyright &copy;<?php echo date("Y"); ?> <?php echo get_bloginfo('description'); ?> - All Rights Reserved</p>
                </div> <!-- .footer-logo -->
                <div class="col-12 footer-info">
                    <?php
                        $new_line = get_option('jcs_info_street');
                        if (get_option('jcs_info_street2')) {
                            $new_line .= ', ' . get_option('jcs_info_street2') . ' | ';
                        } else {
                            $new_line .= ' | ';
                        }
                        $new_line .= get_option('jcs_info_city') . ', ' . get_option('jcs_info_state') . ' ' . get_option('jcs_info_zip') . ' | ';
                        $new_line .= get_option('jcs_contact_email_to') . ' | ';
                        $new_line .= get_option('jcs_info_tollfree');
                    ?>
                    <p>
                        <?php echo $new_line; ?>
                    </p>
                </div>
            </div> <!-- .row -->
        </div> <!-- .container -->

    </footer>

    <?php wp_footer(); ?>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '<?php echo get_option('jcs_options_gtag_id'); ?>');
    </script>

<!--    <script
      type="text/javascript"
      src="https://use.fontawesome.com/releases/v5.15.3/js/conflict-detection.js">
    </script>
-->
  </body>
</html>
