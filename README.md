National Disaster Resilience Conference README.md
============================

Custom WordPress theme and plugin development for the Federal Alliance for Safe Homes (FLASH)
National Disaster Resilience Conference website.

* Theme JCS NDRC
* Plugin(s) JCS Bring the Tweets, JCS Extend Folders
* Version 1.0

### How do I get set up? ###
* WordPress >= 5.8.2
* PHP ~=7.4

### Required WordPress Plugins ###
* Advanced Custom Fields - Delicious Brains
* Ajax Search Lite - Ernest Marcinko
* Custom Post Type UI - WebDevStudios
* Folders - Premio

### Recommended WordPress Plugins ###
* Disable Gutenberg - Jeff Star
* Post Types Order - Nsp Code
* Regenerate Thumbnails - Alex Mills
* Simply Show IDs - Matt Martz

### Dependencies ###
* Debounce CSS Library
* FluidBox CSS/JS Library
* FontAwesome
* RoyalSlider CSS/JS Library

### Contact Info ###
* DriveJCS (https://jdacsolutions.com)
